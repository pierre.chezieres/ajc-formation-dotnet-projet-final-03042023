﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dtos.api
{
    /// <summary>
    /// Enum qui represente le statut d'un stream
    /// </summary>
    public enum StreamStatusType
    {
        InProgress,
        Upcoming,
        Ended
    }
}
