﻿using Stream = twitch.api.Stream;

namespace dtos.api
{
    /// <summary>
    /// Decorateur de la classe stream qui ajoute la propriété Status pour notifier 
    /// du statut du stream
    /// </summary>
    public class StreamDTO
    {
        private Stream stream;

        public StreamDTO() { }

        public StreamDTO(Stream stream) 
        {
            this.Stream = stream;
        }

        public Stream Stream { get => stream; set => stream = value; }

        public StreamStatusType Status
        {
            get
            {
                StreamStatusType status = StreamStatusType.InProgress;
                if (this.Stream.Start >= DateTime.Now.AddSeconds(1) || this.Stream.Start == null)
                {
                    status = StreamStatusType.Upcoming;
                }
                else if (this.Stream.End != null)
                {
                    status = StreamStatusType.Ended;
                }
                return status;
            }
        }
    }
}