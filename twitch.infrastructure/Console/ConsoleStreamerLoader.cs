﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using twitch.api;
using twitch.api.Adapters;
using twitch.infrastructure.Exceptions;

namespace twitch.infrastructure.Console
{
    /// <summary>
    /// Classe de chargement du streamer depuis l'app console
    /// </summary>
    public class ConsoleStreamerLoader : IStreamerLoader<Streamer>
    {
        private Streamer streamer;

        public ConsoleStreamerLoader(Streamer streamer)
        {
            this.streamer = streamer;
        }

        public Streamer Get()
        {
            return streamer;
        }
    }
}
