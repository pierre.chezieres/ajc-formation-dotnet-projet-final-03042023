﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using twitch.api;
using twitch.api.Adapters;
using Formatting = Newtonsoft.Json.Formatting;

namespace twitch.infrastructure.Console
{
    /// <summary>
    /// Classe de sauvegarde de followers dans un fichier json
    /// </summary>
    public class JsonFollowersSaver : ISaver<Follower>
    {
        private readonly string filePath;

        public JsonFollowersSaver(string filePath)
        {
            this.filePath = filePath;
        }

        public string FilePath => filePath;

        public void Add(Follower follower)
        {
            string json = JsonConvert.SerializeObject(follower, Formatting.Indented);
            File.AppendAllText(FilePath, json);
        }

        public void SaveAll(List<Follower> followers)
        {
            string json = JsonConvert.SerializeObject(followers, Formatting.Indented);
            File.WriteAllText(FilePath, json); ;
        }

        public void Remove(int id)
        {
            throw new NotImplementedException();
        }
    }
}
