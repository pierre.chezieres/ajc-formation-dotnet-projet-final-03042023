﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using twitch.api;
using twitch.api.Adapters;
using twitch.infrastructure.Exceptions;

namespace twitch.infrastructure.Console
{
    /// <summary>
    /// Classe de stockage des cadeaux dans une FollowerList pour une app console
    /// </summary>
    public class ConsoleGiftSaver : ISaver<Gift>
    {
        private GiftList gifts;

        public ConsoleGiftSaver(GiftList gifts)
        {
            this.Gifts = gifts;
        }

        public GiftList Gifts { get => gifts; set => gifts = value; }

        public void Add(Gift gift)
        {
            if (Gifts.Any(g => g.Id == gift.Id ))
            {
                throw new DuplicateGiftIdException();
            }
            Gifts.Add(gift);
        }

        public void AddAll(IEnumerable<Gift> objs)
        {
            throw new NotImplementedException();
        }

        public void Remove(int id)
        {
            if (!Gifts.Any(g => g.Id == id))
            {
                throw new UnknownGiftIdException();
            }
            Gifts.Remove(Gifts.FirstOrDefault(gift => gift.Id == id));
        }

        public void SaveAll(List<Gift> objs)
        {
            throw new NotImplementedException();
        }
    }
}
