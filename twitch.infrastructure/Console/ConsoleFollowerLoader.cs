﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using twitch.api;
using twitch.api.Adapters;
using twitch.infrastructure.Exceptions;

namespace twitch.infrastructure.Console
{
    /// <summary>
    /// Classe de chargement des followers depuis l'app console
    /// </summary>
    public class ConsoleFollowerLoader : ILoader<Follower, FollowerList>
    {
        private FollowerList followers;

        public ConsoleFollowerLoader(FollowerList followers)
        {
            this.Followers = followers;
        }
        public FollowerList Followers { get => followers; set => followers = value; }

        public Follower Get(int id)
        {
            var follower = Followers.FirstOrDefault(follower => follower.Id == id);
            if (follower == null)
            {
                throw new UnknownFollowerIdException();
            }
            return follower;
  
        }

        public FollowerList GetAll()
        {
            return Followers;
        }
    }
}
