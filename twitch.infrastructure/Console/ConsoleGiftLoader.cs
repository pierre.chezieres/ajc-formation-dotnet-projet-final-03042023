﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using twitch.api;
using twitch.api.Adapters;
using twitch.infrastructure.Exceptions;

namespace twitch.infrastructure.Console
{
    /// <summary>
    /// Classe de chargement des cadeaux depuis l'app console
    /// </summary>
    public class ConsoleGiftLoader : ILoader<Gift, GiftList>
    {
        private GiftList gifts;

        public ConsoleGiftLoader(GiftList gifts)
        {
            this.Gifts = gifts;
        }

        public GiftList Gifts { get => gifts; set => gifts = value; }


        public Gift Get(int id)
        {
            var gift = Gifts.FirstOrDefault(gift => gift.Id == id);
            if (gift == null) 
            {
                throw new UnknownGiftIdException();
            }
            return gift;
        }

        public GiftList GetAll()
        {
            return Gifts;
        }
    }
}
