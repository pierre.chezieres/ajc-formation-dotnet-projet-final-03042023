﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using twitch.api;
using twitch.api.Adapters;
using twitch.infrastructure.Exceptions;

namespace twitch.infrastructure.Console
{
    /// <summary>
    /// Classe de stockage des cadeaux dans une GiftList pour une app console
    /// </summary>
    public class ConsoleStreamerSaver : IStreamerSaver<Streamer>
    {
        private Streamer streamer;

        public ConsoleStreamerSaver(Streamer streamer)
        {
            Streamer = streamer;
        }

        public Streamer Streamer { get => streamer; set => streamer = value; }

        public void Save(Streamer streamer)
        {
            this.Streamer = streamer;
        }
    }
}
