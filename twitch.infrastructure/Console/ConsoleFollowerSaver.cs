﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using twitch.api;
using twitch.api.Adapters;
using twitch.infrastructure.Exceptions;

namespace twitch.infrastructure.Console
{
    /// <summary>
    /// Classe de stockage des followers dans une FollowerList pour une app console
    /// </summary>
    public class ConsoleFollowerSaver : ISaver<Follower>
    {
        private FollowerList followers;

        public ConsoleFollowerSaver(FollowerList followers)
        {
            this.Followers = followers;
        }

        public FollowerList Followers { get => followers; set => followers = value; }

        public void Add(Follower follower)
        {
            if (this.Followers.Any(s => s.Id == follower.Id))
            {
                throw new DuplicateSubscriberIdException();
            }
            this.Followers.Add(follower);
        }

        public void AddAll(IEnumerable<Follower> objs)
        {
            throw new NotImplementedException();
        }

        public void Remove(int id)
        {
            if (!this.Followers.Any(s => s.Id == id))
            {
                throw new UnknownFollowerIdException();
            }
            Followers.Remove(Followers.FirstOrDefault(gift => gift.Id == id));
        }

        public void SaveAll(List<Follower> objs)
        {
            throw new NotImplementedException();
        }
    }
}
