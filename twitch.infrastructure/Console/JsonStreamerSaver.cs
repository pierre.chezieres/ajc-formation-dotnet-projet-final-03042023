﻿using Newtonsoft.Json;
using Org.BouncyCastle.Utilities.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using twitch.api;
using twitch.api.Adapters;

namespace twitch.infrastructure.Console
{
    /// <summary>
    /// Classe de sauvegarde du streamer dans un fichier json
    /// </summary>
    public class JsonStreamerSaver : ISaver<Streamer>
    {
        private readonly string filePath;

        public JsonStreamerSaver(string filePath)
        {
            this.filePath = filePath;
        }

        public string FilePath => filePath;

        public void Add(Streamer streamer)
        {
            string json = JsonConvert.SerializeObject(streamer, Formatting.Indented);
            File.AppendAllText(FilePath, json); 
        }

        public void SaveAll(List<Streamer> streamers)
        {
            string json = JsonConvert.SerializeObject(streamers, Formatting.Indented);
            File.WriteAllText(FilePath, json);
        }

        public void Remove(int id)
        {
            throw new NotImplementedException();
        }
    }
}
