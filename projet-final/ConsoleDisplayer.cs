﻿using twitch.api.Adapters;


namespace projet_final_bis
{
    /// <summary>
    /// Classe d'affichage sur la sortie standard de la console
    /// </summary>
    internal class ConsoleDisplayer : IDisplayer
    {
        public void Display(string message)
        {
            Console.WriteLine(message);
        }
    }
}
