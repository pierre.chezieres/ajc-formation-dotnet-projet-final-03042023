﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using twitch.api.Adapters;
using twitch.api;
using twitch.infrastructure.Console;
using System.Security.Cryptography.X509Certificates;
using System.Xml.Serialization;

namespace menu.api
{
    /// <summary>
    /// Classe qui represente le menu de notre application console
    /// </summary>
    public class MenuTwitch
    {
        /// <summary>
        /// distionnaire des actions du menu
        /// </summary>
        private Dictionary<int, Action> dictActionMenu;
        /// <summary>
        /// Le menu crée à partir de menu api
        /// </summary>
        private Menu menu;

        /// <summary>
        /// La classe d'affichage
        /// </summary>
        private IDisplayer displayer;

        /// <summary>
        /// La classe de lecture
        /// </summary>
        private IReader reader;

        /// <summary>
        /// Classe d'input/outpout pour les cadeaux
        /// </summary>
        private ILoader<Gift, GiftList> giftLoader;
        private ISaver<Gift> giftSaver;

        /// <summary>
        /// Classe d'input/outpout pour les followers
        /// </summary>
        private ILoader<Follower, FollowerList> followerLoader;
        private ISaver<Follower> followerSaver;

        /// <summary>
        /// Classe d'input/outpout pour le streamer
        /// </summary>
        private IStreamerLoader<Streamer> streamerLoader;
        private IStreamerSaver<Streamer> streamerSaver;

        /// <summary>
        /// Classes de lecture ecriture dans un fichier json
        /// </summary>
        private JsonFollowersSaver jsonFollowersSaver;
        private JsonStreamerSaver jsonStreamersSaver;
        private JsonGiftsSaver jsonGiftsSaver;

        private bool stop;

        public Dictionary<int, Action> DictActionMenu { get => dictActionMenu; set => dictActionMenu = value; }
        public Menu Menu { get => menu; set => menu = value; }
        public IDisplayer Displayer { get => displayer; set => displayer = value; }
        public IReader Reader { get => reader; set => reader = value; }
        public ILoader<Gift, GiftList> GiftLoader { get => giftLoader; set => giftLoader = value; }
        public ISaver<Gift> GiftSaver { get => giftSaver; set => giftSaver = value; }
        public ILoader<Follower, FollowerList> FollowerLoader { get => followerLoader; set => followerLoader = value; }
        public ISaver<Follower> FollowerSaver { get => followerSaver; set => followerSaver = value; }
        public IStreamerLoader<Streamer> StreamerLoader { get => streamerLoader; set => streamerLoader = value; }
        public IStreamerSaver<Streamer> StreamerSaver { get => streamerSaver; set => streamerSaver = value; }
        public bool Stop { get => stop; set => stop = value; }

        public MenuTwitch
            (
                IDisplayer displayer,
                IReader reader,
                ILoader<Gift, GiftList> giftLoader,
                ISaver<Gift> giftSaver,
                ILoader<Follower, FollowerList> followerLoader,
                ISaver<Follower> followerSaver,
                IStreamerLoader<Streamer> streamerLoader,
                IStreamerSaver<Streamer> streamerSaver
            )
        {
            this.Displayer = displayer;
            this.Reader = reader;
            this.GiftLoader = giftLoader;
            this.GiftSaver = giftSaver;
            this.FollowerLoader = followerLoader;
            this.FollowerSaver = followerSaver;
            this.StreamerLoader = streamerLoader;
            this.StreamerSaver = streamerSaver;

            jsonFollowersSaver = new(Path.Combine(Environment.CurrentDirectory, "followers.txt"));
            jsonGiftsSaver = new(Path.Combine(Environment.CurrentDirectory, "gifts.txt"));
            jsonStreamersSaver = new(Path.Combine(Environment.CurrentDirectory, "streamers.txt"));

            Stop = false;

            this.Menu = new(displayer, "==== Twitch ====");
            Item afficherStreamers = new(1, "Afficher streamer", 1);
            Item afficherSubs = new(2, "Afficher followers", 2);
            Item afficherCadeaux = new(3, "Afficher cadeaux", 3);
            Item ajouterSub = new(4, "Ajouter follower", 4);
            Item creerCadeau = new(5, "Creer cadeau", 5);
            Item distribuer = new(6, "Distribuer les cadeaux", 6);
            Item sauvegarder = new(7, "Sauvegarder", 7);
            Item quitter = new(8, "Quitter", 8);


            Menu.AddItem(afficherStreamers);
            Menu.AddItem(afficherSubs);
            Menu.AddItem(afficherCadeaux);
            Menu.AddItem(ajouterSub);
            Menu.AddItem(creerCadeau);
            Menu.AddItem(distribuer);
            Menu.AddItem(sauvegarder);
            Menu.AddItem(quitter);



            DictActionMenu = new()
            {
                {1, AfficherStreamer },
                {2, AfficherFollowers },
                {3, AfficherCadeaux },
                {4, AjouterFollower },
                {5, CreerCadeau },
                {6, Distribuer },
                {7, Sauvegarder },
                {8, () => {Stop = true; } }
            };
        }


        /// <summary>
        /// Fonction qui represente la boucle principale du menu
        /// </summary>
        public void Run()
        {
            do
            {
                Menu.Display();
                if (int.TryParse(reader.Read(), out int choice))
                {
                    if (dictActionMenu.TryGetValue(choice, out Action actionMenu))
                    {
                        actionMenu();
                    }
                    else
                    {
                        Displayer.Display("Saisie invalide.");
                    }
                }
                else
                {
                    Displayer.Display("Sasie invalide.");
                }
            } while (!stop);
        }

        /// <summary>
        /// Methode d'affichage du streamer
        /// </summary>
        public void AfficherStreamer()
        {
            Displayer.Display(StreamerLoader.Get().ToString());
        }

        /// <summary>
        /// Methode d'affichage des followers
        /// </summary>
        public void AfficherFollowers()
        {
            FollowerLoader.GetAll().ForEach(follower =>
            {
                Displayer.Display(follower.ToString());
                follower.Gifts.ForEach(gift => Displayer.Display(gift.ToString()));
            });
        }

        /// <summary>
        /// Methode d'affichage des cadeaux
        /// </summary>
        public void AfficherCadeaux()
        {
            GiftLoader.GetAll().ForEach(gift =>
            {
                Displayer.Display(gift.ToString());
            });
        }

        /// <summary>
        /// Methode d'ajout d'un follower à la liste 
        /// </summary>
        public void AjouterFollower()
        {
            Displayer.Display("Pseudo ?");
            string pseudo = reader.Read();

            Displayer.Display("Email ?");
            string email = reader.Read();


            Follower newFollower = new(pseudo, email);

            followerSaver.Add(newFollower);
            Displayer.Display("Follower ajouté !");
        }

        /// <summary>
        /// Methode de creation d'un cadeau et d'ajout à la liste
        /// </summary>
        public void CreerCadeau()
        {
            displayer.Display("Type de cadeau ?");
            displayer.Display($"{(int)Discount.category}. {Discount.description}");
            displayer.Display($"{(int)GameLesson.category}. {GameLesson.description}");
            displayer.Display($"{(int)Offer.category}. {Offer.description}");
            displayer.Display($"{(int)StreamerMeeting.category}. {StreamerMeeting.description}");

            Dictionary<int, Func<Gift>> getNewGift = new()
            {
                {1, () => { return new Discount(); } },
                {2, () => { return new GameLesson(); } },
                {3, () => { return new Offer (); } },
                {4, () => { return new StreamerMeeting (); } }
            };

            bool validInput = false;

            do
            {
                if (int.TryParse(Reader.Read(), out int giftCategory))
                {
                    if (getNewGift.TryGetValue(giftCategory, out Func<Gift> createGift))
                    {
                        Gift newGift = createGift();
                        GiftSaver.Add(newGift);
                        Displayer.Display("Cadeau ajouté !");
                        validInput = true;
                    }
                    else
                    {
                        Displayer.Display("Saisie invalide.");
                        validInput = false;
                    }
                }
                else
                {
                    Displayer.Display("Saisie invalide.");
                    validInput = false;
                }
            } while (!validInput);
        }
         
        /// <summary>
        /// Methode qui distribue les cadeaux
        /// Utilise un GiftDispatcher
        /// </summary>
        public void Distribuer()
        {
            var gifts = GiftLoader.GetAll();
            var followers = FollowerLoader.GetAll();
            new RandomGiftDispatcher().Dispatch(gifts, followers);
        }

        /// <summary>
        /// Methode de sauvegarde dans un fichier
        /// Utilise un JsonGiftsSaver, JsonStreamersSaver et un JsonFollowersSaver
        /// </summary>
        public void Sauvegarder()
        {
            var gifts = GiftLoader.GetAll();
            var followers = FollowerLoader.GetAll();
            var streamer = StreamerLoader.Get();

            jsonGiftsSaver.SaveAll(gifts);
            jsonStreamersSaver.Add(streamer);
            jsonFollowersSaver.SaveAll(followers);

            Displayer.Display("Enregistrement terminé!");
            Displayer.Display($"Streamer: {jsonStreamersSaver.FilePath}");
            Displayer.Display($"Gifts: {jsonGiftsSaver.FilePath}");
            Displayer.Display($"Followers: {jsonFollowersSaver.FilePath}");

        }
    }
}
