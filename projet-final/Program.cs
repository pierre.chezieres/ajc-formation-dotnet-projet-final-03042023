﻿
using twitch.api;
using menu.api;
using twitch.infrastructure.Console;
using twitch.api.Adapters;
using projet_final_bis;
using Org.BouncyCastle.Math.EC;

ConsoleDisplayer displayer = new();
var reader = new ConsoleReader();



Streamer streamer = new(1, "Pierre", "pierre@....");


var gifts = new GiftList()
{
        new Discount(),
        new GameLesson (),
        new GameLesson (),
        new Offer (),
        new Discount (),
        new GameLesson (),
        new Discount (),
        new StreamerMeeting ()
};

var followers = new FollowerList()
{
    new Follower("Jean", "...@..."),
    new Follower("Julie", "...@..."),
    new Follower("Paul", "...@..."),
    new Follower("Lea", "...@..."),
    new Follower("Thomas", "...@..."),
    new Follower("Charlotte", "...@..."),
    new Follower("Julien", "...@..."),
    new Follower("Mylene", "...@..."),
    new Follower("Mathieu", "...@..."),
    new Follower("Zoé", "...@..."),
    new Follower("Jules", "...@..."),
    new Follower("Alice", "...@..."),

};

ILoader<Gift, GiftList> giftLoader = new ConsoleGiftLoader(gifts);
ILoader<Follower, FollowerList> followerLoader = new ConsoleFollowerLoader(followers);
IStreamerLoader<Streamer> streamerLoader = new ConsoleStreamerLoader(streamer);
ISaver<Gift> giftSaver = new ConsoleGiftSaver(gifts);
ISaver<Follower> followerSaver = new ConsoleFollowerSaver(followers);
IStreamerSaver<Streamer> streamerSaver = new ConsoleStreamerSaver(streamer);

var menu = new MenuTwitch
(
    displayer,
    reader,
    giftLoader,
    giftSaver,
    followerLoader,
    followerSaver,
    streamerLoader,
    streamerSaver
);

menu.Run();




