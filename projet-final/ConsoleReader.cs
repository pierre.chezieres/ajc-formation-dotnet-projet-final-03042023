﻿using twitch.api.Adapters;

namespace projet_final_bis
{
    /// <summary>
    /// Classe de lecture sur l'entrée standard de la console
    /// </summary>
    internal class ConsoleReader : IReader
    {
        public string? Read()
        {
            return Console.ReadLine();
        }
    }
}
