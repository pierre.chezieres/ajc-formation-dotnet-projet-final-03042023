﻿using dtos.api;
using twitch.api;
using Microsoft.AspNetCore.Mvc;
using twitch_simulator.Models;
using Microsoft.AspNetCore.Authorization;

namespace twitch_simulator.Controllers
{
    /// <summary>
    /// Controlleur followers
    /// Gere les interations avec le modele follower ou dérivés et la vue List et Edit de Follower 
    /// </summary>
    public class FollowersController : Controller
    {
        private readonly DefaultDbContext context;
        public FollowersController(DefaultDbContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Action de creation d'un nouveau follower et ajout au contexte
        /// Appel de la vue List
        /// </summary>
        /// <returns>Retourne vers la vue List</returns>
        [Authorize]
        [HttpPost]
        public IActionResult New()
        {
            this.context.Followers.Add(new Follower());
            this.context.SaveChanges();
            return RedirectToAction("List");
        }

        /// <summary>
        /// Action qui appelle la vue Edit de Follower
        /// recupere l'id du follower grace à la mathode GET
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Retourne vers la vue Edit</returns>
        [Authorize]
        [HttpGet]
        public IActionResult Edit(int id)
        {
            Follower follower = this.context.Followers.Find(id);
            return View("Edit", follower);
        }


        /// <summary>
        /// Action de mise a jour d'un follower du contexte
        /// </summary>
        /// <param name="editFollower"></param>
        /// <returns>Appel de l'action liste</returns>
        [Authorize]
        [HttpPost]
        public IActionResult Edit(Follower editFollower)
        {
            Follower follower = this.context.Followers.Find(editFollower.Id);
            follower.Pseudo = editFollower.Pseudo;
            follower.Email = editFollower.Email;
            follower.ProfilePicture = editFollower.ProfilePicture;
            this.context.SaveChanges();
            return RedirectToAction("List");
        }

        /// <summary>
        /// Action de suppression d'un follower de la liste dans le contexte
        /// </summary>
        /// <param name="id">identitfiaant du follower</param>
        /// <returns>Appel de l'action List</returns>
        [Authorize]
        [HttpGet]
        public IActionResult Delete(int id)
        {
            Follower follower = this.context.Followers.Find(id);
            this.context.Followers.Remove(follower);
            this.context.SaveChanges();
            return RedirectToAction("List");
        }


        /// <summary>
        /// Action qui appelle la vue liste apres avoir recupere les followers du contexte
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public IActionResult List()
        {
            var query = from follower in this.context.Followers select follower;
            return View("List", query.ToList());
        }

        /// <summary>
        /// Action qui fait la liste des followers apres une mise a jour du statut d'un follower 
        /// </summary>
        /// <param name="editFollower"></param>
        /// <returns>Retourne vers la vue List</returns>
        [Authorize]
        [HttpPost]
        public IActionResult List(Follower editFollower)
        {
            var follower = this.context.Followers.Find(editFollower.Id);
            if (follower.IsConnected)
            {
                var currentFollowerSession = this.context.Sessions.FirstOrDefault(session => session.FollowerId == follower.Id && session.End == null);
                currentFollowerSession.End = DateTime.Now;
                follower.IsConnected = false;
            }
            else
            {
                this.context.Sessions.Add(new Session(follower.Id));
                follower.IsConnected = true;
            }
            this.context.SaveChanges();
            return RedirectToAction("List");
        }
    }
}
