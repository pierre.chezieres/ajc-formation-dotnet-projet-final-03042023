﻿using dtos.api;
using Stream = twitch.api.Stream;
using Microsoft.AspNetCore.Mvc;
using twitch_simulator.Models;
using Microsoft.AspNetCore.Authorization;

namespace twitch_simulator.Controllers
{
    public class StreamController : Controller
    {
        private readonly DefaultDbContext context;
        public StreamController(DefaultDbContext context)
        {
            this.context = context;
        }

        [Authorize]
        public IActionResult List()
        {
            var query = from stream in this.context.Streams select new StreamDTO(stream);
             

            return View("List", query.ToList());
        }

        [Authorize]
        [HttpGet]
        public IActionResult Stop(int id)
        {
            Stream stream = this.context.Streams.Find(id);
            stream.End = DateTime.Now;
            this.context.SaveChanges();
            return RedirectToAction("List");
        }

        [Authorize]
        [HttpGet]
        public IActionResult Start(int id)
        {
            Stream stream = this.context.Streams.Find(id);
            stream.Start = DateTime.Now;
            stream.End = null;
            this.context.SaveChanges();
            return RedirectToAction("List");
        }

        [Authorize]
        [HttpPost]
        public IActionResult New()
        {
            this.context.Streams.Add(new Stream());
            this.context.SaveChanges();
            return RedirectToAction("List");
        }
        [Authorize]
        [HttpGet]
        public IActionResult Delete(int id)
        {
            Stream stream = this.context.Streams.Find(id);
            this.context.Streams.Remove(stream);
            this.context.SaveChanges();
            return RedirectToAction("List");
        }


        [Authorize]
        [HttpGet]
        public IActionResult Edit(int id)
        {
            Stream stream = this.context.Streams.Find(id);
            return View("Edit", stream);
        }

        [Authorize]
        [HttpPost]
        public IActionResult Edit(Stream editStream)
        {
            Stream stream = this.context.Streams.Find(editStream.Id);
            stream.Title = editStream.Title;
            stream.Start = editStream.Start;
            stream.End = null;
            this.context.SaveChanges();
            return RedirectToAction("List");
        }


    }
}
