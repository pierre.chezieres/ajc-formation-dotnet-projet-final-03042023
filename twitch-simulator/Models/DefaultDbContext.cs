﻿using Microsoft.EntityFrameworkCore;
using System;
using twitch.api;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;


namespace twitch_simulator.Models
{
    public class DefaultDbContext : DbContext
    {
        public DbSet<Streamer> Streamers { get; set; }
        public DbSet<Follower> Followers { get; set; }
        public DbSet<twitch.api.Stream> Streams { get; set; }
        public DbSet<twitch.api.Session> Sessions { get; set; }

        public DefaultDbContext(DbContextOptions<DefaultDbContext> options) : base(options)
        {
        }

        protected DefaultDbContext()
        {
        }
    }
}
