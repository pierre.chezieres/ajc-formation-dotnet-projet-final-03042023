﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace twitch.api
{
    /// <summary>
    /// Classe qui implement IGiftDispatcher
    /// Represente la distribution aleatoire de cadeau (au plus un cadeau par follower et par tirage)
    /// </summary>
    public class RandomGiftDispatcher : IGiftDispatcher
    {
        private static readonly Random random = new Random();

        /// <summary>
        /// Fonction qui represente un systeme de distribution aleatoire de cadeaux
        /// En respectant la contrainte: au plus 1 cadeau par follower, par tirage
        /// </summary>
        /// <param name="gifts">La liste des cadeaux eventuellement distribués</param>
        /// <param name="followers">La liste des followers eventuellement destinataires</param>
        public void Dispatch(GiftList gifts, FollowerList followers)
        {
            var shuffledFollowers = followers.OrderBy(follower => random.Next()).Take(gifts.Count).ToList();
            var shuffledGifts = gifts.OrderBy(gift => random.Next()).Take(followers.Count).ToList();
            int index = 0;
            shuffledGifts.ForEach(gift =>
            {
                shuffledFollowers[index].Gifts.Add(gift);
                index++;
            });
            gifts.RemoveAll(gift => shuffledGifts.Contains(gift));
        }
    }
}
