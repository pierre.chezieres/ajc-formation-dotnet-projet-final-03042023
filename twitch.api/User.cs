﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace twitch.api
{
    /// <summary>
    /// Classe mère qui represente un utilisateur de twitch (follower ou streamer)
    /// </summary>
    public abstract class User
    {
        private static int count = 0;
        private int id;
        private string? pseudo;
        private string? email;

        public User() 
        {
        }

        public User(string pseudo, string email)
        {
            this.Id = count++;
            this.pseudo = pseudo;
            this.email = email;
        }

        public User(int id, string pseudo, string email)
        {
            this.Id = id;
            this.Pseudo = pseudo;
            this.Email = email;
        }

        public override string ToString()
        {
            return $"{this.Id} {this.Pseudo}";
        }

        public int Id { get => id; set => id = value; }
        public string? Pseudo { get => pseudo; set => pseudo = value; }
        public string? Email { get => email; set => email = value; }
    }
}
