﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace twitch.api
{
    /// <summary>
    /// Interface qui represente la distribution des cadeau par la fonction dispatch
    /// Chaque systeme de distribution est une classe qui implemente cette interface
    /// </summary>
    public interface IGiftDispatcher
    {
        /// <summary>
        /// Prototype de la fonction Dispatch qui represente la distribution de cadeaux
        /// </summary>
        /// <param name="gifts">La liste des cadeaux</param>
        /// <param name="followers">La liste des followers</param>
        public void Dispatch(GiftList gifts, FollowerList followers);
    }
}
