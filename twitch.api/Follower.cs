﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace twitch.api
{
    /// <summary>
    /// Classe qui represente un follower
    /// </summary>
    public class Follower : User
    {
        private static readonly string DEFAULT_PICTURE_PATH = "/ProfilePictures/picture";
        private static readonly int NB_PICTURES = 10;
        private static readonly Random random = new Random();
        private GiftList gifts;
        private string? profilePicture;
        private bool isConnected;

        public Follower()
        {
            this.Gifts = new GiftList();
            this.IsConnected = false;
            int i = random.Next(NB_PICTURES);
            this.profilePicture = $"{DEFAULT_PICTURE_PATH}{i}.png";
        }

        public Follower(string pseudo, string email) : base(pseudo, email)
        {
            this.Gifts = new GiftList();
            this.IsConnected = false;
        }

        public Follower(int id, string pseudo, string email) : base(id, pseudo, email)
        {
            this.Gifts = new GiftList();
            this.IsConnected = false;
        }

        public override string ToString()
        {
            return $"[Follower] {base.ToString()}";
        }

        public bool IsConnected { get => isConnected; set => isConnected = value; }

        [NotMapped]
        public GiftList Gifts { get => gifts; set => gifts = value; }
        public string? ProfilePicture { get => profilePicture; set => profilePicture = value; }
    }
}
