﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace twitch.api
{
    /// <summary>
    /// Classe abstraite qui represente un cadeau
    /// </summary>
    public abstract class Gift
    {

        private static int count = 0;
        private int id;
        private DateTime creationDate;

        public Gift()
        {
            count++;
            this.Id = count;
            creationDate = DateTime.Now;
        }

        public Gift(int id)
        {
            this.Id = id;
            creationDate = DateTime.Now;
        }

        public override string ToString()
        {
            return $"[Gift] {this.Id}." ;
        }

        public int Id { get => id; set => id = value; }
        public DateTime CreationDate { get => creationDate; set => creationDate = value; }
    }
}
