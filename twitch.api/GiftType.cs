﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace twitch.api
{
    /// <summary>
    /// Enum qui represente les category de cadeau
    /// </summary>
    public enum GiftType
    {
        Discount = 1,
        GameLesson = 2,
        Offer = 3,
        StreamerMeeting = 4,
    }
}
