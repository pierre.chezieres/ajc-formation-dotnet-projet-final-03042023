﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace twitch.api.Adapters
{
    public interface ISaver<T>
    {
        public void Add(T obj);
        public void SaveAll(List<T> objs);
        public void Remove(int id);

    }
}
