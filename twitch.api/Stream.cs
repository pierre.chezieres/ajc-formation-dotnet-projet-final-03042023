﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace twitch.api
{
    /// <summary>
    /// Classe qui représente un stream
    /// </summary>
    public class Stream
    {
        private int id;
        private string title;
        /// <summary>
        /// Date de creation du stream
        /// </summary>
        private DateTime creationDate;
        /// <summary>
        /// Date de demarrage du stream
        /// Si null => le stream n'a pas commencé
        /// </summary>
        private DateTime? start;
        /// <summary>
        /// Date de fin du stream
        /// Si null => le stream n'est pas terminé ou n'a pas commencé
        /// </summary>
        private DateTime? end;


        public Stream()
        {
            this.CreationDate = DateTime.Now;
            this.Title = "New";
        }

        public Stream(int id, string title, DateTime? start, DateTime? end)
        {
            this.Id = id;
            this.CreationDate = DateTime.Now;
            this.Title = title;
            this.Start = start;
            this.End = end;
        }

        public int Id { get => id; set => id = value; }
        public string Title { get => title; set => title = value; }
        public DateTime? Start { get => start; set => start = value; }
        public DateTime? End { get => end; set => end = value; }
        public DateTime CreationDate { get => creationDate; set => creationDate = value; }
    }
}
