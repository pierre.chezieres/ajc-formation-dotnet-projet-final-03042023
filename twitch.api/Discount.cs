﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace twitch.api
{
    /// <summary>
    /// Classe qui représente le cadeau "reduction"
    /// </summary>
    public class Discount : Gift 
    {
        public static readonly GiftType category = GiftType.Discount;
        public static readonly string description = "Réduction(promo sur un site de gaming)";

        public override string ToString()
        {
            return $"{base.ToString()}{category}";
        }
    }
}
