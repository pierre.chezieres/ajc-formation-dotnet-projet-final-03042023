﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace twitch.api
{
    /// <summary>
    /// Classe qui represente la cadeau "Rencontre avec le streamer"
    /// </summary>
    public class StreamerMeeting : Gift
    {
        public static readonly GiftType category = GiftType.StreamerMeeting;
        public static readonly string description = "Passer du temps avec le Streamer";

        public override string ToString()
        {
            return $"{base.ToString()}{category}";
        }
    }
}
