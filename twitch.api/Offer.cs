﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace twitch.api
{
    /// <summary>
    /// Classe qui represente un cadeau "offre commerciale"
    /// </summary>
    public class Offer : Gift
    {
        public static readonly GiftType category = GiftType.Offer;
        public static readonly string description = "Réduc de 100% sur un jeu vidéo";

        public override string ToString()
        {
            return $"{base.ToString()}{category}";
        }
    }
}
