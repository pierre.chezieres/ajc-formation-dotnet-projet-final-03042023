﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace twitch.api
{
    public class Streamer : User
    {
        public Streamer(int id, string pseudo, string email) : base(id, pseudo, email) { }

        public override string ToString()
        {
            return $"[Streamer] {base.ToString()}";
        }
    }
}
