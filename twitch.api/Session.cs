﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace twitch.api
{
    /// <summary>
    /// Classe qui represente une session de connection d'un follower
    /// Il peut se connecter à un stream spécifiaue ou non
    /// </summary>
    public class Session
    {

        private int id;
        private int? streamId;
        private int followerId;
        /// <summary>
        /// Date de debut de la connection
        /// Si nul => le follower n'est pas connecté
        /// </summary>
        private DateTime start;
        /// <summary>
        /// Date de fin de connection
        /// Si null => la connection est en cours
        /// </summary>
        private DateTime? end;

        public Session(int followerId) 
        {
            this.StreamId = null;
            this.FollowerId = followerId;
            this.Start = DateTime.Now;
            this.End = null;
        }

        public Session(int? streamId, int followerId, DateTime start)
        {
            this.StreamId = streamId;
            this.FollowerId = followerId;
            this.Start = start;
            this.End = null;
        }

        public int Id { get => id; set => id = value; }
        public int? StreamId { get => streamId; set => streamId = value; }
        public int FollowerId { get => followerId; set => followerId = value; }
        public DateTime Start { get => start; set => start = value; }
        public DateTime? End { get => end; set => end = value; }
    }
}
