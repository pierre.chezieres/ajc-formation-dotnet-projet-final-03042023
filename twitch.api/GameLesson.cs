﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace twitch.api
{
    /// <summary>
    /// Classe qui represente un cadeau "Lesson de jeu"
    /// </summary>
    public class GameLesson : Gift
    {
        public static readonly GiftType category = GiftType.GameLesson;
        public static readonly string description = "Apprendre à bien jouer sur un jeu";

        public override string ToString()
        {
            return $"{base.ToString()}{category}";
        }
    }
}
