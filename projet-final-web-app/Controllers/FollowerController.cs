﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
using projet_final_web_app.Models;


namespace projet_final_web_app.Controllers
{
    /// <summary>
    /// Classe FollowerController associé aux interactions avec les followers du contexte
    /// </summary>
    public class FollowerController : Controller
    {
        private readonly DefaultDbContext context;
        public FollowerController(DefaultDbContext context)
        {
            this.context = context;
        }
        /// <summary>
        /// Action de listage des follower du contexte
        /// </summary>
        /// <returns>Retourne vers la vue Liste</returns>
        public IActionResult List()
        {
            var query = from follower in this.context.Followers
                        select follower;
            return View(query.ToList());
        }

        /// <summary>
        /// Action de listage des follower connectés
        /// </summary>
        /// <returns>Retourne vers la vue list</returns>
        public IActionResult ConnectionList()
        {
            var query = from follower in this.context.Followers
                        where follower.IsConnected == true
                        select follower;
            return View("List", query.ToList());
        }

        /// <summary>
        /// Action de listage des followers les plus actifs ou bien de l 'ensemble des followers selon le parametre option
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Retourne vers la vue Activity</returns>
        public IActionResult FollowersActivity(FollowersActivityModel model)
        {
            if (model.Option == OptionType.All)
            {
                var followersQuery = from follower in this.context.Followers
                                     select follower;
                model.Followers = followersQuery.ToList();
            }

            if (model.Option == OptionType.Active)
            {
                int nbStream = this.context.Streams.Count();

                var filteredSessions = from session in this.context.Sessions
                                      where session.StreamId != null
                                      select session;

                var activeFollowersIdQuery = from session in filteredSessions
                                             group session by session.FollowerId into followerGroup
                                             where followerGroup.Count() / nbStream > 0.7
                                             select followerGroup.Key;

                var activeFollowersQuery = from follower in this.context.Followers
                                           where activeFollowersIdQuery.Contains(follower.Id)
                                           select follower;

                model.Followers = activeFollowersQuery.ToList();
            }

            return View("Activity", model);
        }
    }
}
