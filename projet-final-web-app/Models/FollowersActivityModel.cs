﻿using Microsoft.AspNetCore.Mvc;
using twitch.api;

namespace projet_final_web_app.Models
{
    public class FollowersActivityModel
    {
        public List<Follower> Followers { get; set; }

        [BindProperty]
        public OptionType Option { get; set; }
    }
}
