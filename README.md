# ajc-formation-dotnet-projet-final-03042023


twitch.api: la biblihoteque de classe principale
    classes:
        User
        Follower
        Streamer
        Stream
        Session
        Gift
        ...

ConsoleApp:
    L'application console de la premiere partie du projet

twitch.infrastructure:
    Les classes d'I/O de la console App
    (Pour enregistrement dans un fichier json notamment)


WebApp:
    Projet Web:
    - Listage des followers
    - Listage des followers connectés
    - Listage des followers les plus actifs


TwitchSimulator :
    Projet Web de la partie 3
     - Affichage et edition des streams
     - Simulation des connexions des followers
     - Connection avec le package Nuget Identity et EF.Facebook




